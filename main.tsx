import { h, render } from 'preact'
import Layer from './src/Layer'
import Neuron from './src/Neuron'
import Network from './src/Network'
import GraphComponent from './components/Graph'

let spd = ({x, w}) => {
    return x[0] * w[0] + x[1] * w[1]
}

let spd2 = ({x, w}) => {
    return x[0] * w[0] + x[1] * w[1] + x[2] * w[2] + x[3] * w[3] + x[4] * w[4] + x[5] * w[5]
}

let inputs: Layer = new Layer([
    new Neuron([], spd),
    new Neuron([], spd)
])

let layers = [
    new Layer([
        new Neuron([0, 1], spd),
        new Neuron([0, 1], spd),
        new Neuron([0, 1], spd),
        new Neuron([0, 1], spd),
        new Neuron([0, 1], spd),
        new Neuron([0, 1], spd)
    ]),
    new Layer([
        new Neuron([0, 1, 2, 3, 4, 5], spd2),
        new Neuron([0, 1, 2, 3, 4, 5], spd2),
        new Neuron([0, 1, 2, 3, 4, 5], spd2),
        new Neuron([0, 1, 2, 3, 4, 5], spd2),
        new Neuron([0, 1, 2, 3, 4, 5], spd2),
        new Neuron([0, 1, 2, 3, 4, 5], spd2),
    ]),
]

let outputs = new Layer([
    new Neuron([0, 1, 2, 3, 4, 5], spd2),
    new Neuron([0, 1, 2, 3, 4, 5], spd2)
])

inputs[0].value = 1231231
inputs[1].value = 3

let network = new Network(inputs, layers, outputs)
network.process()
render(<GraphComponent network={network}/>, document.querySelector('#app'))
