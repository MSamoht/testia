import { h, Component } from 'preact'
import Network from '../src/Network'

interface IState {
    network: Network
}

interface IPorps {
    network: Network
}

export default class GraphComponent extends Component<IPorps, IState> {

    constructor(props) {
        super(props)
        this.state = {
            network: props.network
        }
    }

    get inputs() {
        let inputs = []
        this.state.network.inputs.forEach(n => {
            inputs.push(<div class={`neuron ${+ n.selected ? 'selected' : ''}`}>{n.value}</div>)
        })
        return inputs
    }

    getFromLayer(id: number) {
        let layer = []
        this.state.network.layers[id].forEach((n, id) => {
            layer.push(<div class={`neuron ${+ n.selected ? 'selected' : ''}`}>{n.value}</div>)
        })
        return layer
    }

    get layers() {
        let layers = []
        this.state.network.layers.forEach((layer, id) => {
            layers.push(<div class="block">{this.getFromLayer(id)}</div>)
        })
        return layers
    }

    get outputs() {
        let outputs = []
        this.state.network.outputs.forEach(n => {
            outputs.push(<div class={`neuron ${+ n.selected ? 'selected' : ''}`}>{n.value}</div>)
        })
        return outputs
    }

    get synapses() {
        let synapses = []
        this.state.network.inputs.forEach(n => {
            synapses.push(...n.synapses)
        })
        this.state.network.layers.forEach(l => {
            l.forEach(n => {
                synapses.push(...n.synapses)
            })
        })
        this.state.network.outputs.forEach(n => {
            synapses.push(...n.synapses)
        })
        return synapses
    }

    overSynapse(s) {
        s.left.selected = true
        s.right.selected = true
        this.forceUpdate()
    }

    outSynapse(s) {
        s.left.selected = false
        s.right.selected = false
        this.forceUpdate()
    }

    get synapsesElement() {
        let synapses = []
        this.synapses.forEach(s => {
            synapses.push(<tr onMouseOver={(e) => this.overSynapse(s)} onMouseOut={(e) => this.outSynapse(s)}>
                <td>{s.left}</td>
                <td>{s.weight}</td>
                <td>{s.right}</td>
            </tr>)
        })
        return synapses
    }
    
    render({}, { network }): JSX.Element {
        return <div>
            <div class="block">
                {this.inputs}
            </div>
            {this.layers}
            <div class="block">
                {this.outputs}
            </div>
            <table>
                <tbody>
                    {this.synapsesElement}
                </tbody>
            </table>
        </div>
    }
}