const path = require('path')
const webpack = require('webpack')
const HtmlWebpackPlugin = require('html-webpack-plugin')

let config = {
    entry: ['./main.tsx'],
    output: {
        path: path.resolve(__dirname, 'dist'),
        publicPath: '/',
        filename: 'main.js'
    },
    devtool: "inline-source-map",
    resolve: {
        extensions: ['.js', '.ts', '.tsx'],
        alias: {
            src: path.resolve(__dirname, 'src'),
            '@': path.resolve(__dirname, 'src/resources'),
            components: path.resolve(__dirname, 'src/components')
        }
    },
    devServer: {
        historyApiFallback: true
    },
    module: {
        rules: [{
            test: /\.ts|tsx$/,
            loader: 'ts-loader'
        }]
    },
    plugins: [
        new HtmlWebpackPlugin({
            template: '!!html-loader!index.html',
            inject: true
        })
    ]
}

if (process.env.NODE_ENV === 'production') {
    config.plugins.push(new webpack.DefinePlugin({
        'process.env': {
            NODE_ENV: '"production"'
        }
    }))
    config.plugins.push(new PrerenderSpaPlugin(
        path.join(__dirname, 'dist'),
        ['/', '/galeries']
    ))
}
module.exports = config