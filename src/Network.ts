import Neuron from './Neuron'
import Layer from './Layer'
import Synapse from './Synapse'

export default class Network {


    constructor(public inputs: Layer, public layers: Layer[], public outputs: Layer) {
        for (let l in layers) {
            this.createSynapsesForLayer(parseInt(l), layers[l])
        }
        this.createSynapsesForLayer(0, outputs, true)
    }


    createSynapsesForLayer(id: number, layer: Layer, isOutput: boolean = false) {
        let preLayer: Layer
        preLayer = (id === 0) ? this.inputs : this.layers[id - 1]
        if (isOutput) { preLayer = this.layers.slice(-1)[0] }
        layer.forEach((neuron: Neuron, i: number) => {
            for(let n in neuron.needed) {
                let synapse = new Synapse(Math.random(), preLayer[neuron.needed[n]], neuron)
                neuron.synapses.push(synapse)
            }
        })
    }

    process() {
        this.layers.forEach(l => {
            l.forEach(n => {
                n.process()
            })
        })
        this.outputs.forEach(n => {
            n.process()
        })
    }
    
    learn(target: number[], rate: number, alpha: number): void {
        this.outputs.forEach((neuron: Neuron, id: number) => {
            neuron.delta = target[id] - neuron.value
        })
        this.setDeltaForLayers(alpha)
        this.setWeights(rate, alpha)
    }

    setDeltaForLayers(alpha: number): void {      
        for (let layerId = this.layers.length - 1; layerId >= 0; layerId--) {
            let layer = this.layers[layerId]
            let nextLayer = (layerId === this.layers.length - 1) ? this.outputs : this.layers[layerId + 1]
            layer.forEach((neuron: Neuron) => {
                let synapses: Synapse[] = []
                nextLayer.forEach((nextNeuron: Neuron) => {
                    synapses.push(nextNeuron.getSynapseConnectedTo(neuron))
                })
                let delta = alpha * neuron.value * (1 - neuron.value)
                let sum = 0
                synapses.forEach((synapse: Synapse) => {
                    sum += synapse.weight * synapse.right.delta
                })
                neuron.delta = delta + sum
            })
        }
    }

    setWeights(rate: number, alpha: number): void {
        this.layers.forEach((layer: Layer) => {
            layer.forEach((neuron: Neuron) => {
                neuron.synapses.forEach((synapse: Synapse) => {
                    let omega = this.getOmega(synapse, rate, alpha)
                    synapse.weight += omega
                    synapse.omega = omega
                })
            })
        })
        this.outputs.forEach((neuron: Neuron) => {
            neuron.synapses.forEach((synapse: Synapse) => {
                let omega = this.getOmega(synapse, rate, alpha)
                synapse.weight += omega
                synapse.omega = omega
            })
        })
    }

    getOmega(synapse: Synapse, rate: number, alpha: number): number {
        return rate * synapse.right.value * synapse.right.delta * alpha + (1 - alpha) * synapse.omega
    }

}

