import Synapse from './Synapse'

export default class Neuron {

    public synapses: Synapse[] = []
    public value: number = 0
    public delta: number = 0

    constructor(public needed: number[], private internal: (args: { x: any, w: any }) => number) {

    }

    get args() {
        let synapses: Synapse[] = []
        this.needed.forEach(n => {
            synapses.push(this.synapses[n])
        })
        let x = {}
        let w = {}
        synapses.forEach((synapse: Synapse, id: number) => {
            x[this.needed[id]] = synapse.left.value
            w[this.needed[id]] = synapse.weight
        })
        return { x, w }
    }

    process(): void {
        this.value = this.sigmo(this.internal(this.args))
    }

    sigmo(x: number): number {
        return 1 / (1 + Math.exp(-x))
    }

    getSynapseConnectedTo(neuron: Neuron): Synapse {
        return this.synapses.filter((synapse: Synapse) => synapse.left === neuron)[0]
    }

}