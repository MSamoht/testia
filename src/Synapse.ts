import Neuron from './Neuron'

export default class Synapse {

    public omega: number = 0

    constructor(public weight: number, public left: Neuron, public right: Neuron) {
        
    }
    
}