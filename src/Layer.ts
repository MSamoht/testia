import Neuron from './Neuron'

export default class Layer extends Array {

    constructor(neurons: Neuron[]) {
        super(0)
        this.push(...neurons)
    }

}