import { expect } from 'chai'
import Neuron from '../src/Neuron'
import Layer from '../src/Layer'
import Network from '../src/Network'

let inputs = new Layer([
    new Neuron([], () => 1),
    new Neuron([], () => 1),
    new Neuron([], () => 1)
])

let layers = [
    new Layer([
        new Neuron([0, 1], () => 1),
        new Neuron([0, 1, 2], () => 1),
        new Neuron([1, 2], () => 1),
        new Neuron([0, 2], () => 1),
    ]),
    new Layer([
        new Neuron([1], () => 1),
        new Neuron([1, 2, 3], () => 1),
        new Neuron([0, 2], () => 1),
        new Neuron([1, 3], () => 1),
        new Neuron([2, 3], () => 1),
    ]),
]

let outputs = new Layer([
    new Neuron([0, 1, 2], () => 1),
    new Neuron([0, 2, 3], () => 1),
    new Neuron([1, 2, 3, 4], () => 1)
])
let network: Network = new Network(inputs, layers, outputs)
describe('Network', () => {

    it('should have right number of synapse', () => {
        expect(network.layers[0][0].synapses.length).to.be.equal(2)
        expect(network.layers[1][1].synapses.length).to.be.equal(3)
        expect(network.outputs[2].synapses.length).to.be.equal(4)
        expect(network.outputs[1].synapses.length).to.be.equal(3)
    })

    it('should have the right neurons connected', () => {
        expect(network.layers[0][0].synapses[0].left).to.be.deep.equal(network.inputs[0])
        expect(network.layers[0][0].synapses[0].right).to.be.deep.equal(network.layers[0][0])

        expect(network.layers[1][1].synapses[1].left).to.be.deep.equal(network.layers[0][2])
        expect(network.layers[1][1].synapses[1].right).to.be.deep.equal(network.layers[1][1])

        expect(network.outputs[1].synapses[1].left).to.be.deep.equal(network.layers[1][2])
        expect(network.outputs[1].synapses[1].right).to.be.deep.equal(network.outputs[1])
    })
})